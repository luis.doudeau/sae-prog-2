"""
              Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module case.py
    ce module gère les cases du plateau
"""
import trojan
import protection

def creer_case(fleche, la_protection, serveur, liste_trojans):
    """créer une case du plateau

    Args:
        fleche (str): une des quatre directions 'H' 'B' 'G' 'D' ou '' si pas de flèche sur la case
        la_protection (dict): l'objet de protection posé sur la case (None si pas d'objet)
        serveur (dict): le serveur posé sur la case (None si pas de serveur)
        liste_trojan s(list): la liste des trojans présents sur le case

    Returns:
        dict: la représentation d'une case
    """
    return {"fleche" : fleche , "protection" : la_protection,
    "serveur" : serveur, "trojan_present" : liste_trojans,"trojan_arrivant": [],
    "joueur" : False}
    
    
def get_fleche(case):
    """retourne la direction de la flèche de la case

    Args:
        case (dict): une case

    Returns:
        str: la direction 'H', 'B', 'G', 'D' ou '' si pas de flèche sur la case
    """
    return case["fleche"]


def get_protection(case):
    """retourne l'objet de protection qui se trouve sur la case

    Args:
        case (dict): une case

    Returns:
        dict: l'objet de protection présent sur la case (None si pas d'objet)
    """
    return case["protection"]


def get_serveur(case):
    """retourne le serveur qui se trouve sur la case

    Args:
        case (dict): une case

    Returns:
        dict: le serveur présent sur la case (None si pas d'objet)
    """
    return case["serveur"]


def get_trojans(case):
    """retourne la liste des trojans présents sur la case

    Args:
        case (dict): une case

    Returns:
        list: la liste des trojans présents sur la case
    """
    return case["trojan_present"]

def get_trojans_entrants(case):
    """retourne la liste des trojans qui vont arriver sur la case

    Args:
        case (dict): une case

    Returns:
        list: la liste des trojans qui vont arriver sur la case
    """
    return case["trojan_arrivant"]

def set_fleche(case, direction):
    """affecte une direction à la case

    Args:
        case (dict): une case
        direction (dict): 'H', 'B', 'G', 'D' ou '' si pas de flèche sur la case
    """
    case["fleche"] = direction
 

def set_serveur(case, serveur):
    """affecte un serveur à la case

    Args:
        case (dict): une case
        serveur (str): le serveur
    """
    case["serveur"] = serveur

def set_protection(case, la_protection):
    """affecte une protection à la case

    Args:
        case (dict): une case
        la_protection (dict): la protection
    """
    case["protection"] = la_protection


def set_les_trojans(case, trojans_presents, trojans_entrants):
    """fixe la liste des trojans présents et les trojans arrivant sur la case

    Args:
        case (dict): une case
        trojans_presents (list): une liste de trojans
        trojans_entrants ([type]): une liste de trojans
    """
    case["trojan_present"] = trojans_presents
    case["trojan_arrivant"] = trojans_entrants



def ajouter_trojan(case, un_trojan):
    """ajouter un nouveau trojan arrivant à une case

    Args:
        case (dict): la case
        un_trojan (dict): le trojan à ajouter
    """
    case["trojan_arrivant"].append(un_trojan)

def mettre_a_jour_case(case):
    """met les trojans arrivants comme présents et réinitialise les trojans arrivants
       change la direction du trojan si nécessaire (si la case comporte une flèche)
       la fonction retourne un dictionnaire qui indique pour chaque numéro de joueur
       le nombre de trojans qui vient d'arriver sur la case.
       La fonction enlève une resistance à protection qui se trouve sur elle et la détruit si
       la protection est arrivée à 0.

    Args:
        case (dict): la case

    Returns:
        dict: un dictionnaire dont les clés sont les numéros de joueur et les valeurs
              le nombre de trojans arrivés sur la case pour ce joueur
    """
    res = {1:0,2:0,3:0,4:0}
    case["trojan_present"] = case["trojan_arrivant"]
    for trojan_case in case["trojan_present"]:
        if get_fleche(case) != "":
            trojan.set_direction(trojan_case,get_fleche(case))
        protection_case = get_protection(case)
        if protection_case is not None:
            protection.enlever_resistance(protection_case)
        res[trojan_case["createur"]] += 1
    case["trojan_arrivant"] = []
    
    return res

def poser_avatar(case):
    """pose l'avatar sur cette case et élimines les trojans présents sur la case.
       la fonction indique combien de trojans ont été éliminés

    Args:
        case (dict): une case

    Returns:
        dict: un dictionnaire dont les clés sont les numéros de joueur et les valeurs le nombre
        de trojans éliminés pour ce joueur.
    """
    dico_elimine = {1:0,2:0,3:0,4:0}
    case["joueur"] = True
    for trojan_present in case["trojan_present"]:
        dico_elimine[trojan.get_createur(trojan_present)] += 1
    return dico_elimine
        



def enlever_avatar(case):
    """enlève l'avatar de la case

    Args:
        case (dict): une case
    """
    case["joueur"] = False


def contient_avatar(case):
    """vérifie si l'avatar se trouve sur la case

    Args:
        case (dict): une case

    Returns:
        bool: True si l'avatar est sur la case et False sinon[type]: [description]
    """
    if "joueur" not in case.keys():
        return False
    else:
        return case["joueur"]


def reinit_trojans_entrants(case):
    """reinitialise la liste des trojans entrants à la liste vide

    Args:
        case ([type]): [description]
    """
    case["trojan_arrivant"] = []