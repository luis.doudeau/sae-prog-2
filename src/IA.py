from operator import le
import random
import trojan
import plateau
import protection
import joueur
import matrice
import equipement
import jeu
import case


def voisin(ligne, colonne): 
    res = []
    if 4 >= ligne-1 >= 0 and 4 >= colonne >= 0 :
        res.append((ligne-1, colonne))
    if 4 >= ligne+1 >= 0 and 4 >= colonne >= 0 :
        res.append((ligne+1, colonne))
    if 4 >= ligne >= 0 and 4 >= colonne-1 >= 0 :
        res.append((ligne, colonne-1))
    if 4 >= ligne >= 0 and 4 >= colonne+1 >= 0 :
        res.append((ligne, colonne+1))
    return res

def verif_menace_direct(le_jeu, id_joueur): 
    le_plateau = jeu.get_plateau(le_jeu, id_joueur)
    voisin_serveur = voisin(plateau.get_taille(le_plateau)//2, plateau.get_taille(le_plateau)//2)
    res = False
    for voisins_direct in voisin_serveur :
        if case.get_trojans(plateau.get_case(le_plateau, voisins_direct[0], voisins_direct[1])) != [] : 
            return True
    return res

def verif_menace_indirect(le_jeu, id_joueur): 
    le_plateau = jeu.get_plateau(le_jeu, id_joueur)
    voisin_serveur = voisin(plateau.get_taille(le_plateau)//2, plateau.get_taille(le_plateau)//2)
    res = False
    for voisins_indirect in voisin_serveur :
        if case.get_trojans_entrants(plateau.get_case(le_plateau, voisins_indirect[0], voisins_indirect[1])) != [] : 
            return True
    return res


def menace_direct(le_jeu, id_joueur):
    le_plateau = jeu.get_plateau(le_jeu, id_joueur)
    voisin_serveur = voisin(plateau.get_taille(le_plateau)//2, plateau.get_taille(le_plateau)//2)
    max = None
    res = None
    for voisins in voisin_serveur :
        if max is None or len(case.get_trojans(plateau.get_case(le_plateau, voisins[0], voisins[1]))) > max : 
            res = voisins
            max = len(case.get_trojans(plateau.get_case(le_plateau, voisins[0], voisins[1])))
    return matrice.DICO_DIR[(res[0]-(plateau.get_taille(le_plateau)//2), res[1]-(plateau.get_taille(le_plateau)//2))]


def menace_indirect(le_jeu, id_joueur):
    le_plateau = jeu.get_plateau(le_jeu, id_joueur)
    voisin_serveur = voisin((plateau.get_taille(le_plateau)//2, plateau.get_taille(le_plateau)//2))
    max = None
    res = None
    for voisins in voisin_serveur :
        if max is None or len(case.get_trojans_entrants(plateau.get_case(le_plateau, voisins[0], voisins[1]))) > max : 
            res = voisins
            max = len(case.get_trojans_entrants(plateau.get_case(le_plateau, voisins[0], voisins[1])))
    return matrice.DICO_DIR[(res[0]-(plateau.get_taille(le_plateau)//2), res[1]-(plateau.get_taille(le_plateau)//2))]


def deplacement_avatar(le_jeu, id_joueur) :
    le_plateau = jeu.get_plateau(le_jeu, id_joueur) 
    if plateau.get_pos_avatar(le_plateau) != (plateau.get_taille(le_plateau)//2, plateau.get_taille(le_plateau)//2) : 
        next_pos = matrice.DICO_DIR[(plateau.get_taille(le_plateau)//2 - plateau.get_pos_avatar(le_plateau)[0], plateau.get_taille(le_plateau)//2 - plateau.get_pos_avatar(le_plateau)[1])]
        return next_pos
    elif verif_menace_direct(le_jeu, id_joueur) : 
        return menace_direct(le_jeu, id_joueur)
    elif not(verif_menace_direct(le_jeu, id_joueur)) and verif_menace_indirect(le_jeu, id_joueur) :
        return menace_indirect(le_jeu, id_joueur)
    else : 
        return 'HH'


def verif_menace_ordis_indirect(le_jeu, id_joueur): 
    le_plateau = jeu.get_plateau(le_jeu, id_joueur)
    res = False
    for i in range(0,5) : 
        if case.get_trojans(plateau.get_case(le_plateau, plateau.get_taille(le_plateau)-2, i) ) != []:
            for trojans in case.get_trojans(plateau.get_case(le_plateau, plateau.get_taille(le_plateau)-2, i) ):
                if trojan.get_direction(trojans) == "B":
                    return True

    if case.get_trojans(plateau.get_case(le_plateau,plateau.get_taille(le_plateau)-1,0)) != []:
        for trojans in case.get_trojans(plateau.get_case(le_plateau,plateau.get_taille(le_plateau)-1,0)):
            if trojan.get_direction(trojans) == "D":
                return True

    if case.get_trojans(plateau.get_case(le_plateau,plateau.get_taille(le_plateau)-1,0)) != []:
        for trojans in case.get_trojans(plateau.get_case(le_plateau,0,plateau.get_taille(le_plateau)-1)):
            if trojan.get_direction(trojans) == "G":
                return True
    return res


def menace_ordis_indirect(le_jeu, id_joueur) :
    res = 0
    le_plateau = jeu.get_plateau(le_jeu, id_joueur) 
    le_plateau2 = le_plateau.copy()
    cpt = 0
    for position,cases in plateau.get_matrice(le_plateau).items():
        if position[0] == plateau.get_taille(le_plateau)-2:
            for trojans in case.get_trojans(cases):
                if trojan.get_direction(trojans) == "G" or trojan.get_direction(trojans) == "D":
                    cpt+=1
    if cpt > 2:
        if plateau.poser_protection(le_plateau2,4,4,2):
            return "442"
        elif plateau.poser_protection(le_plateau2,0,4,2):
            return "042"
    

    if case.get_trojans(plateau.get_case(le_plateau,plateau.get_taille(le_plateau)-2,0)) != []:
        for trojans in case.get_trojans(plateau.get_case(le_plateau,plateau.get_taille(le_plateau)-2,0)):
            if trojan.get_direction(trojans) == "B":
                res = 1
                if plateau.poser_protection(le_plateau2,4,4,0):
                    return "442"

    if case.get_trojans(plateau.get_case(le_plateau,plateau.get_taille(le_plateau)-2,plateau.get_taille(le_plateau)-1)) != []:
        for trojans in case.get_trojans(plateau.get_case(le_plateau,plateau.get_taille(le_plateau)-2,plateau.get_taille(le_plateau)-1)):
            if trojan.get_direction(trojans) == "B":
                res = -1
                if plateau.poser_protection(le_plateau2,4,4,4):
                    return "444"
    if res == 1:
        if plateau.poser_protection(le_plateau2,4,3,1):
            return "433"
        else:
            return "333"
    else:
        if plateau.poser_protection(le_plateau2,4,3,3):
            return "431"
        else:
            return "331"


def verif_menace_ordis_direct(le_jeu, id_joueur) :
    le_plateau = jeu.get_plateau(le_jeu, id_joueur)
    res = False
    for i in range(0,5) : 
        if le_plateau["matrice"][(plateau.get_taille(le_plateau)-1, i)]["liste_trojans_present"] != [] : 
            return True
    return res


def menace_ordis_direct(le_jeu, id_joueur) :
    le_plateau = jeu.get_plateau(le_jeu, id_joueur) 
    max_voisin = None
    for i in range(0,plateau.get_taille(le_plateau)) :
        voisins = voisin(plateau.get_taille(le_plateau)-1, i)
        if max_voisin is None or len(le_plateau["matrice"][voisins]["liste_trojans_present"]) > max_voisin : 
            (ligne_v, colonne_v) = (plateau.get_taille(le_plateau)-1, i)
            max_voisin = len(le_plateau["matrice"][voisins]["liste_trojans_present"])
    return str(ligne_v)+str(colonne_v)


def nb_trojan_plateau(le_jeu, id_joueur) : 
    le_plateau = jeu.get_plateau(le_jeu, id_joueur)
    dico_trojan_prochain = plateau.nb_trojans_prochain(le_plateau)
    res = 0
    for (_, nb_troj) in dico_trojan_prochain.items() : 
        res += nb_troj
    return res


def nb_trojan_autre_plateau(le_jeu) : 
    """ Cette fonction renvoie le nombre de trojans que le joueurs 
    possèdent sur les autres plateaux du jeu. Nombres de trojan lui appartenant vivant
    
    Args :  
    
    Returns : int : le nombre de trojans appartenant au joueur sur tout les autre plateaux"""

    res=0 
    for i in range (1,5) :

        res += nb_trojan_plateau(le_jeu, i)
    return None # pas bon

def verif_attaque_serveurs():
    """ Verifie si il ya plus de 3 attaques """
    pass

def resitance_actuelle(le_jeu, id_joueur):
    le_plateau = jeu.get_plateau(le_jeu, id_joueur)
    pass

def choix_att_def(le_jeu, id_joueur) :  
    le_plateau = jeu.get_plateau(le_jeu, id_joueur)
    # id_joueur_gauche = plateau.id_joueur_gauche(le_plateau)
    # le_plateau_gauche = jeu.get_plateau(le_jeu, id_joueur_gauche)
    # id_joueur_gauche = plateau.id_joueur_gauche(le_plateau)
    # le_plateau_droit = jeu.get_plateau(le_jeu, id_joueur_gauche)
    # le_plateau = jeu.get_plateau(le_jeu, id_joueur)
    # id_joueur_gauche = plateau.id_joueur_gauche(le_plateau)
    # le_plateau_gauche = jeu.get_plateau(le_jeu, id_joueur_gauche) 

    if verif_menace_ordis_indirect(le_jeu, id_joueur) or choix_poser_dp(le_jeu, id_joueur):
            return 'P'
    # elif jeu.get_num_tour(le_jeu) >= 10 and plateau.get_points(le_plateau) :
    #     pass
    #    return 'P'
    else :
        return 'A'

# max_matrice : regarde le nombre de trojans max qu'il ya sur la case voisine #########
# le trojan est un tours sur deux sur le serveurs 
# parcourir le plateau et trouver la case qui a le plus de voisin avec le plus de trojans.
# si pas de menace et plus de 5 trojans autour de case
# envoie trojan : trouver les lignes et colonnes les moins frequentés
# fonction qui renvoie si on attaque ou si on defend, avec menace.
# fonction poser piege qui regarde d'ou la menace provient et reagi en posant une piege
# fonction avatar qui va faire bouger l'avatar sur la case voisine du serveur avec le plus de trojan arrivant 
# si avatar pas sur la case du serveur, il y retourne.
# fonction : les case des plus menacante qui prend en parametre une plateau et qui renvoie la case voisine la plus menacante.
# fonction qui regarde si il ya des données personnelles et qui ordonne de bouger l'avatar dessus 
# fonction qui pose un antivirus sur la case sur serveur detruit (trou noire) si lavatar n'est pas dessus. (position 2,2)

def poser_dp(le_jeu, id_joueur) : 
    le_plateau = jeu.get_plateau(le_jeu, id_joueur)
    max_total_trojan = None
    voisin_serveur = matrice.voisin(plateau.get_taille(le_plateau)//2, plateau.get_taille(le_plateau)//2)
    for (ligne, colonne) in voisin_serveur :
        max_voisin = 0
        for voisins_dp in voisin(ligne, colonne) :
            max_voisin += len(case.get_trojans(plateau.get_case(le_plateau,voisins_dp[0], voisins_dp[1])))
        if max_total_trojan is None or max_voisin > max_total_trojan : 
            max_total_trojan = max_voisin
            max_voisin = 0
            (lignes_v, colonne_v) = (ligne, colonne)
    return str(protection.DONNEES_PERSONNELLES) + str(lignes_v) + str(colonne_v)


def choix_poser_dp(le_jeu, id_joueur) :
    le_plateau = jeu.get_plateau(le_jeu, id_joueur)
    for voisins in matrice.voisin(plateau.get_taille(le_plateau)//2, plateau.get_taille(le_plateau)//2) :
        if len(case.get_trojans(plateau.get_case(le_plateau,voisins[0], voisins[1]))) >= 2 and plateau.get_pos_avatar(le_plateau) != (voisins[0], voisins[1]) and len(case.get_trojans(plateau.get_case(le_plateau,voisins[0], voisins[1]))) == [] : 
            return True
    return False

def choix_def(le_jeu, id_joueur) : 
    if not(verif_menace_ordis_indirect(le_jeu, id_joueur)) and choix_poser_dp(le_jeu, id_joueur) : 
        return poser_dp(le_jeu, id_joueur)
    else : 
        return menace_ordis_indirect(le_jeu, id_joueur)


def attaque_gauche(le_jeu, id_joueur) :
    le_plateau = jeu.get_plateau(le_jeu, id_joueur)
    id_joueur_gauche = plateau.id_joueur_gauche(le_plateau)
    le_plateau_gauche = jeu.get_plateau(le_jeu, id_joueur_gauche)
    if plateau.get_pos_avatar(le_plateau_gauche)[0] <= 1  and case.get_protection(plateau.get_case(le_plateau_gauche, 4, 4)) is None :
        return 'G4'
    elif plateau.get_pos_avatar(le_plateau_gauche)[0] <= 1  and case.get_protection(plateau.get_case(le_plateau_gauche, 4, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_gauche, 3, 4)) is None :
        return 'G3'
    elif plateau.get_pos_avatar(le_plateau_gauche)[0] <= 1  and case.get_protection(plateau.get_case(le_plateau_gauche, 4, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_gauche, 3, 4)) is not None :
        return 'G2'
    elif plateau.get_pos_avatar(le_plateau_gauche)[0] >= 3  and plateau.get_pos_avatar(le_plateau_gauche)[1] <= 1 and case.get_protection(plateau.get_case(le_plateau_gauche, 0, 4)) is None :
        return 'G0'
    elif plateau.get_pos_avatar(le_plateau_gauche)[0] >= 3  and plateau.get_pos_avatar(le_plateau_gauche)[1] <= 1 and case.get_protection(plateau.get_case(le_plateau_gauche, 0, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_gauche, 4, 4)) is None :
        return 'G4'
    elif plateau.get_pos_avatar(le_plateau_gauche)[0] >= 3  and plateau.get_pos_avatar(le_plateau_gauche)[1] <= 1 and case.get_protection(plateau.get_case(le_plateau_gauche, 0, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_gauche, 4, 4)) is not None :
        return 'G3'
    elif plateau.get_pos_avatar(le_plateau_gauche)[0] >= 3  and plateau.get_pos_avatar(le_plateau_gauche)[1] >=3 and case.get_protection(plateau.get_case(le_plateau_gauche, 0, 4)) is None :
        return 'G0'
    elif plateau.get_pos_avatar(le_plateau_gauche)[0] >= 3  and plateau.get_pos_avatar(le_plateau_gauche)[1] >=3 and case.get_protection(plateau.get_case(le_plateau_gauche, 0, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_gauche, 1, 4)) is None :
        return 'G2'
    elif plateau.get_pos_avatar(le_plateau_gauche)[0] >= 3  and plateau.get_pos_avatar(le_plateau_gauche)[1] >=3 and case.get_protection(plateau.get_case(le_plateau_gauche, 0, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_gauche, 1, 4)) is not None  :
        return 'G1'
    elif plateau.get_pos_avatar(le_plateau_gauche) == (0,2) or plateau.get_pos_avatar(le_plateau_gauche) == (1,2) and case.get_protection(plateau.get_case(le_plateau_gauche, 4, 4)) is None :
        return 'G4'
    elif plateau.get_pos_avatar(le_plateau_gauche) == (0,2) or plateau.get_pos_avatar(le_plateau_gauche) == (1,2) and case.get_protection(plateau.get_case(le_plateau_gauche, 4, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_gauche, 3, 4)) is None :
        return 'G3'
    elif plateau.get_pos_avatar(le_plateau_gauche) == (0,2) or plateau.get_pos_avatar(le_plateau_gauche) == (1,2) and case.get_protection(plateau.get_case(le_plateau_gauche, 4, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_gauche, 3, 4)) is not None :
        return 'G4'
    elif plateau.get_pos_avatar(le_plateau_gauche) == (2,0) or plateau.get_pos_avatar(le_plateau_gauche) == (2,1) and case.get_protection(plateau.get_case(le_plateau_gauche, 4, 4)) is None :
        return 'G4'
    elif plateau.get_pos_avatar(le_plateau_gauche) == (2,0) or plateau.get_pos_avatar(le_plateau_gauche) == (2,1) and case.get_protection(plateau.get_case(le_plateau_gauche, 4, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_gauche, 0, 4)) is None :
        return 'G0'
    elif plateau.get_pos_avatar(le_plateau_gauche) == (2,0) or plateau.get_pos_avatar(le_plateau_gauche) == (2,1) and case.get_protection(plateau.get_case(le_plateau_gauche, 4, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_gauche, 0, 4)) is not None :
        return 'G3'
    elif plateau.get_pos_avatar(le_plateau_gauche) == (3,2) or plateau.get_pos_avatar(le_plateau_gauche) == (4,2) and case.get_protection(plateau.get_case(le_plateau_gauche, 0, 4)) is None :
        return 'G0'
    elif plateau.get_pos_avatar(le_plateau_gauche) == (3,2) or plateau.get_pos_avatar(le_plateau_gauche) == (4,2) and case.get_protection(plateau.get_case(le_plateau_gauche, 0, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_gauche, 1, 4)) is None :
        return 'G1'
    elif plateau.get_pos_avatar(le_plateau_gauche) == (3,2) or plateau.get_pos_avatar(le_plateau_gauche) == (4,2) and case.get_protection(plateau.get_case(le_plateau_gauche, 0, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_gauche, 1, 4)) is not None :
        return 'G0'
    elif plateau.get_pos_avatar(le_plateau_gauche) == (2,3) or plateau.get_pos_avatar(le_plateau_gauche) == (2,4) and case.get_protection(plateau.get_case(le_plateau_gauche, 4, 4)) is None :
        return 'G4'
    elif plateau.get_pos_avatar(le_plateau_gauche) == (3,2) or plateau.get_pos_avatar(le_plateau_gauche) == (4,2) and case.get_protection(plateau.get_case(le_plateau_gauche, 4, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_gauche, 0, 4)) is None :
        return 'G0'
    elif plateau.get_pos_avatar(le_plateau_gauche) == (3,2) or plateau.get_pos_avatar(le_plateau_gauche) == (4,2) and case.get_protection(plateau.get_case(le_plateau_gauche, 4, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_gauche, 0, 4)) is not None :
        return 'G0'
    else : 
        return 'G4' 


def attaque_droite(le_jeu, id_joueur) : 
    le_plateau = jeu.get_plateau(le_jeu, id_joueur)
    id_joueur_droite = plateau.id_joueur_droite(le_plateau)
    le_plateau_droite = jeu.get_plateau(le_jeu, id_joueur_droite)
    if plateau.get_pos_avatar(le_plateau_droite)[0] <= 1 and case.get_protection(plateau.get_case(le_plateau_droite, 4, 4)) is None :
        return 'D4'
    elif plateau.get_pos_avatar(le_plateau_droite)[0] <= 1  and case.get_protection(plateau.get_case(le_plateau_droite, 4, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_droite, 3, 4)) is None :
        return 'D3'
    elif plateau.get_pos_avatar(le_plateau_droite)[0] <= 1  and case.get_protection(plateau.get_case(le_plateau_droite, 4, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_droite, 3, 4)) is not None :
        return 'D2'
    elif plateau.get_pos_avatar(le_plateau_droite)[0] >= 3  and plateau.get_pos_avatar(le_plateau_droite)[1] <= 1 and case.get_protection(plateau.get_case(le_plateau_droite, 0, 4)) is None :
        return 'D0'
    elif plateau.get_pos_avatar(le_plateau_droite)[0] >= 3  and plateau.get_pos_avatar(le_plateau_droite)[1] <= 1 and case.get_protection(plateau.get_case(le_plateau_droite, 0, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_droite, 4, 4)) is None :
        return 'D4'
    elif plateau.get_pos_avatar(le_plateau_droite)[0] >= 3  and plateau.get_pos_avatar(le_plateau_droite)[1] <= 1 and case.get_protection(plateau.get_case(le_plateau_droite, 0, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_droite, 4, 4)) is not None :
        return 'D3'
    elif plateau.get_pos_avatar(le_plateau_droite)[0] >= 3  and plateau.get_pos_avatar(le_plateau_droite)[1] >=3 and case.get_protection(plateau.get_case(le_plateau_droite, 0, 4)) is None :
        return 'D0'
    elif plateau.get_pos_avatar(le_plateau_droite)[0] >= 3  and plateau.get_pos_avatar(le_plateau_droite)[1] >=3 and case.get_protection(plateau.get_case(le_plateau_droite, 0, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_droite, 1, 4)) is None :
        return 'D2'
    elif plateau.get_pos_avatar(le_plateau_droite)[0] >= 3  and plateau.get_pos_avatar(le_plateau_droite)[1] >=3 and case.get_protection(plateau.get_case(le_plateau_droite, 0, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_droite, 1, 4)) is not None  :
        return 'D1'
    elif plateau.get_pos_avatar(le_plateau_droite) == (0,2) or plateau.get_pos_avatar(le_plateau_droite) == (1,2) and case.get_protection(plateau.get_case(le_plateau_droite, 4, 4)) is None :
        return 'D4'
    elif plateau.get_pos_avatar(le_plateau_droite) == (0,2) or plateau.get_pos_avatar(le_plateau_droite) == (1,2) and case.get_protection(plateau.get_case(le_plateau_droite, 4, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_droite, 3, 4)) is None :
        return 'D3'
    elif plateau.get_pos_avatar(le_plateau_droite) == (0,2) or plateau.get_pos_avatar(le_plateau_droite) == (1,2) and case.get_protection(plateau.get_case(le_plateau_droite, 4, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_droite, 3, 4)) is not None :
        return 'D4'
    elif plateau.get_pos_avatar(le_plateau_droite) == (2,0) or plateau.get_pos_avatar(le_plateau_droite) == (2,1) and case.get_protection(plateau.get_case(le_plateau_droite, 4, 4)) is None :
        return 'D4'
    elif plateau.get_pos_avatar(le_plateau_droite) == (2,0) or plateau.get_pos_avatar(le_plateau_droite) == (2,1) and case.get_protection(plateau.get_case(le_plateau_droite, 4, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_droite, 0, 4)) is None :
        return 'D0'
    elif plateau.get_pos_avatar(le_plateau_droite) == (2,0) or plateau.get_pos_avatar(le_plateau_droite) == (2,1) and case.get_protection(plateau.get_case(le_plateau_droite, 4, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_droite, 0, 4)) is not None :
        return 'D3'
    elif plateau.get_pos_avatar(le_plateau_droite) == (3,2) or plateau.get_pos_avatar(le_plateau_droite) == (4,2) and case.get_protection(plateau.get_case(le_plateau_droite, 0, 4)) is None :
        return 'D0'
    elif plateau.get_pos_avatar(le_plateau_droite) == (3,2) or plateau.get_pos_avatar(le_plateau_droite) == (4,2) and case.get_protection(plateau.get_case(le_plateau_droite, 0, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_droite, 1, 4)) is None :
        return 'D1'
    elif plateau.get_pos_avatar(le_plateau_droite) == (3,2) or plateau.get_pos_avatar(le_plateau_droite) == (4,2) and case.get_protection(plateau.get_case(le_plateau_droite, 0, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_droite, 1, 4)) is not None :
        return 'D0'
    elif plateau.get_pos_avatar(le_plateau_droite) == (2,3) or plateau.get_pos_avatar(le_plateau_droite) == (2,4) and case.get_protection(plateau.get_case(le_plateau_droite, 4, 4)) is None :
        return 'D4'
    elif plateau.get_pos_avatar(le_plateau_droite) == (3,2) or plateau.get_pos_avatar(le_plateau_droite) == (4,2) and case.get_protection(plateau.get_case(le_plateau_droite, 4, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_droite, 0, 4)) is None :
        return 'D0'
    elif plateau.get_pos_avatar(le_plateau_droite) == (3,2) or plateau.get_pos_avatar(le_plateau_droite) == (4,2) and case.get_protection(plateau.get_case(le_plateau_droite, 4, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_droite, 0, 4)) is not None :
        return 'D0'
    else : 
        return 'D4'


def attaque_haut(le_jeu, id_joueur) :
    le_plateau = jeu.get_plateau(le_jeu, id_joueur)
    id_joueur_haut = plateau.id_joueur_haut(le_plateau)
    le_plateau_haut = jeu.get_plateau(le_jeu, id_joueur_haut)
    if plateau.get_pos_avatar(le_plateau_haut)[1] <= 1 :
        return 'H4'
    elif plateau.get_pos_avatar(le_plateau_haut)[1] >= 3 :
        return 'H0'
    else : 
        if case.get_protection(plateau.get_case(le_plateau_haut, 0, 4)) is None :
            return 'H4'
        elif case.get_protection(plateau.get_case(le_plateau_haut, 0, 4)) is not None and case.get_protection(plateau.get_case(le_plateau_haut, 0, 0)) is None :
            return 'H0' 
        else : 
            return 'H4'


def serveur_hs(le_jeu, id_joueur) : # verif que les trojans ne peuvent plus se poser sur la case mais l'avatar et les pieges si, et gagne des points si il ya avatar ou piege
    le_plateau = jeu.get_plateau(le_jeu, id_joueur)
    return plateau.get_resistance_serveur(le_plateau) <= 0


def programme_principal(le_jeu, id_joueur) :
    le_plateau = jeu.get_plateau(le_jeu, id_joueur) 
    if choix_att_def(le_jeu, id_joueur) == 'A' : 
        res = str(deplacement_avatar(le_jeu, id_joueur)) + 'A'
        res += attaque_gauche(le_jeu, id_joueur) + attaque_haut(le_jeu, id_joueur) + attaque_droite(le_jeu, id_joueur)
        return res
    else :
        res = deplacement_avatar(le_jeu, id_joueur) + 'P'
        res += choix_def(le_jeu, id_joueur)
        return res
