"""
             Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module equipement.py
    ce module gère le plateau des joueurs  équipements ordinateurs et serveurs
"""

import matrice
import case
import equipement
import trojan
import protection
import joueur

# direction de déplacement possible pour l'avatar
DIRECTIONS_AVATAR = {'DD', 'GG', 'HH', 'BB', 'HD', 'HG', 'BD', 'BG'}

# A AJOUTER DANS VOTRE FICHIER
# points par action
PENALITE = -5
TROJAN_ELIMINE = 10
TROJAN_PIEGE = 5
DEPLACEMENT_TROJAN = 1
EQUIPEMENT_TOUCHE = -15
# FIN AJOUT

def creer_plateau(id_joueur, nom_joueur, taille_plateau=5, resistance_serveur=4,
                  resistance_pc=50, nb_points=0):
    """créer un nouveau plateau

    Args:
        id_joueur (int): l'identifiant du joueur à qui appartient le plateau
        nom_joueur (str): le nom du joueur à qui appartient le plateau
        taille_plateau (int, optional): la taille du plateau. Defaults to 5.
        resistance_serveur (int, optional): la resistance initiale du serveur. Defaults to 4.
        resistance_pc (int, optional): la resistance initiale des PC. Defaults to 5.
        nb_points (int,optional): le nombre de points du joueur. Defaults to 0.
    Returns:
        dict: un plateau tel que décrit dans le sujet
    """
    plateau_case = matrice.creer_matrice(taille_plateau,taille_plateau,None)
    for cle in plateau_case.keys():
        plateau_case[cle] = case.creer_case("",None,None,[])
    matrice.set_val(plateau_case,taille_plateau//2,taille_plateau//2-1,case.creer_case("D",None,None,[]))
    matrice.set_val(plateau_case,taille_plateau//2,taille_plateau//2+1,case.creer_case("G",None,None,[]))
    matrice.set_val(plateau_case,taille_plateau//2-1,taille_plateau//2,case.creer_case("B",None,None,[]))
    matrice.set_val(plateau_case,taille_plateau//2+1,taille_plateau//2,case.creer_case("H",None,None,[]))
    matrice.set_val(plateau_case,0,taille_plateau//2,case.creer_case("B",None,None,[]))
    matrice.set_val(plateau_case,taille_plateau//2+1,taille_plateau//2,case.creer_case("H",None,None,[]))
    matrice.set_val(plateau_case,taille_plateau//2+2,taille_plateau//2,case.creer_case("B",None,None,[]))
    serveur =equipement.creer_equipement(equipement.SERVEUR,resistance_serveur)
    matrice.set_val(plateau_case,taille_plateau//2,taille_plateau//2,case.creer_case("",None,serveur,[]))
    case.poser_avatar(plateau_case[(taille_plateau//2,taille_plateau//2)])

    return{
    "joueur" : joueur.creer_joueur(id_joueur,nom_joueur,nb_points),
    "matrice" : plateau_case,
    "serveur" : serveur,
    "ordinateur": equipement.creer_equipement(equipement.ORDINATEUR,resistance_pc),
    "trojan_recu" : [],
    "trojan_sortant" : {"H" : [], "B" : [], "G" : [], "D" : []},
    "resistance_protection" : 0
    }

def get_id_joueur(plateau):
    """retourne l'identifiant du joueur à qui appartient le plateau

    Args:
        plateau (dict): un plateau

    Returns:
        int: l'identifiant du joueur
    """
    return joueur.get_id(plateau["joueur"])

def get_nom_joueur(plateau):
    """retourne le nom du joueur à qui appartient le plateau

    Args:
        plateau (dict): un plateau

    Returns:
        str: le nom du joueur
    """
    return joueur.get_nom(plateau["joueur"])


def get_trojans_entrants(plateau):
    """retourne la liste des trojans arrivants sur le plateau

    Args:
        plateau (dict): un plateau

    Returns:
        list: la liste des trojans arrivants sur le plateau
    """
    return plateau["trojan_recu"]


def get_trojans_sortants(plateau, direction):
    """retourne la liste des trojans sortants du plateau pour la destination direction

    Args:
        plateau (dict): un plateau
        direction (str): une des quatre directions H, B, D, G

    Returns:
        list: la liste des trojans sortants du plateau vers la direction indiquée
    """
    return plateau["trojan_sortant"][direction]



def get_taille(plateau):
    """retourne la taille de la matrice du plateau

    Args:
        plateau (dict): un plateau

    Returns:
        int: la taille du plateau
    """
    return matrice.get_nb_colonnes(plateau["matrice"])

def get_case(plateau, ligne, colonne):
    """retourne la case qui se trouve en ligne,colonne sur le plateau

    Args:
        plateau (dict): un plateau
        ligne (int): le numéro de la ligne
        colonne (int): le numéro de la colonne

    Returns:
        dict: une case
    """
    return plateau["matrice"][(ligne,colonne)]


def get_points(plateau):
    """retourne le nombre de points de joueur à qui appartient le plateau

    Args:
        plateau (dict): un plateau

    Returns:
        int: le nombre de points du joueur
    """
    return joueur.get_points(plateau["joueur"])


def get_nb_ordis_restants(plateau):
    """indique combien il reste d'ordinateurs sur le plateau

    Args:
        plateau (dict): un plateau

    Returns:
        int: le nombre d'ordinateurs restants
    """
    return equipement.get_resistance(plateau["ordinateur"])


def get_resistance_serveur(plateau):
    """indique combien de resistance il reste au serveur

    Args:
        plateau (dict): un plateau

    Returns:
        int: le niveau de resistance restant au serveur
    """
    return equipement.get_resistance(plateau["serveur"])


def get_serveur(plateau):
    """retourne le serveur du plateau

    Args:
        plateau (dict): un plateau

    Returns:
        dict: un équipement de type serveur
    """
    return plateau["serveur"]


def get_pos_avatar(plateau):
    """retourne la position de l'avatar sur le plateau

    Args:
        plateau (dict): un plateau

    Returns:
        (int,int): la ligne et la colonne où se trouve l'avatar sur le plateau
    """
    for (ligne_case,colonne_case) in plateau["matrice"].keys():
        if case.contient_avatar(get_case(plateau,ligne_case,colonne_case)):
            return (ligne_case,colonne_case)



def get_ordis(plateau):
    """retourne les ordinateurs du plateau

    Args:
        plateau (dict): un plateau

    Returns:
        dict: un équipement de type ordinateurs
    """
    return plateau["ordinateur"]


def get_matrice(plateau):
    """retourne la matrice associée au plateau

    Args:
        plateau (dict): un plateau

    Returns:
        dict: une matrice
    """
    return plateau["matrice"]


def poser_avatar(plateau, ligne, colonne):
    """pose l'avatar sur la case en ligne,colonne

    Args:
        plateau (dict): un plateau
        ligne (int): le numéro de la ligne
        colonne (int): le numéro de la colonne
    """
    
    plateau["matrice"][(ligne,colonne)]["joueur"] = True
    


def poser_protection(plateau, type_protection, ligne, colonne):
    """pose une protection sur le plateau avec les règles suivantes:
        1. aucune autre protection n'est sur le plateau
        2. la protection n'a pas été détruite
        3. aucun trojan ne se trouve sur la case où on mets la protection
        4. la ligne et la colonne existent
      La fonction retourne True si la pose de la protection s'est bien passée

    Args:
        plateau (dict): un plateau
        type_protection (int): le type de la protection à mettre
        ligne (int): le numéro de la ligne
        colonne (int): le numéro de la colonne

    Returns:
        bool: True si la pose s'est bien passée et False sinon
    """
    if est_sur_le_plateau(plateau,(ligne,colonne)):
        if case.get_trojans(plateau["matrice"][(ligne,colonne)]) != []:
            return False
        
        elif case.contient_avatar(plateau["matrice"][ligne,colonne]):
            return False
        for ligne_case,colonne_case in plateau["matrice"]:
                if case.get_protection(get_case(plateau,ligne_case,colonne_case)) != None :
                    if protection.get_type(case.get_protection(get_case(plateau,ligne_case,colonne_case))) == type_protection:
                        return False
        
        case.set_protection(get_case(plateau,ligne,colonne),protection.creer_protection(type_protection,plateau["resistance_protection"]))   
        return True
    else:
        return False
       
        


def set_trojans_entrants(plateau, les_trojans):
    """Met les trojans entrants sur le plateau

    Args:
        plateau (dict): un plateau
        les_trojans (list): une liste de trojans
    """
    plateau["trojan_recu"] = les_trojans


def set_trojans_sortants(plateau, direction, les_trojans):
    """mets les trojans devant sortir vers la direction indiquée

    Args:
        plateau (dict): un plateau
        direction (str): une des directions H, B, D, G
        les_trojans (list): une liste de trojans
    """
    plateau["trojan_sortant"][direction] = les_trojans

# FIN DES FONCTIONS NECESSAIRES POUR LA SAUVEGARDE/RESTAURATION D'UN PLATEAU


def id_joueur_droite(plateau):
    """retourne l'identifiant du joueur de droite

    Args:
        plateau (dict): un plateau

    Returns:
        int: l'identifiant du joueur de droite
    """
    return joueur.id_joueur_droite(plateau["joueur"])

def id_joueur_gauche(plateau):
    """retourne l'identifiant du joueur de gauche

    Args:
        plateau (dict): un plateau

    Returns:
        int: l'identifiant du joueur de gauche
    """
    return joueur.id_joueur_gauche(plateau["joueur"])

def id_joueur_haut(plateau):
    """retourne l'identifiant du joueur du haut

    Args:
        plateau (dict): un plateau

    Returns:
        int: l'identifiant du joueur du haut
    """
    return joueur.id_joueur_haut(plateau["joueur"])


def ajouter_points(plateau, nb_points):
    """ajoute le nombre de point nb_points au joueur du plateau

    Args:
        plateau (dict): un plateau
        nb_points (int): le nombre de points (éventuellement négatif)

    Returns:
        int: le nombre de points total du joueur du plateau
    """
    joueur.ajouter_points(plateau["joueur"],nb_points)
    return get_points(plateau)

def ajouter_trojan(plateau, liste_trojan):
    """ajoute une liste de noveaux trojan à la liste des trojan qui vont entrer
       sur le plateau lors du prochain tour

    Args:
        plateau (dict): un plateau
        liste_trojan (list): la liste des trojans à ajouter
    """
    plateau["trojan_recu"].extend(liste_trojan)


def attaquer_ordinateurs(plateau):
    """enlève une resistance aux ordinateurs du plateau

    Args:
        plateau (dict): un plateau

    Returns:
        int: le nombre d'ordinateurs restants
    """
    equipement.attaque(plateau["ordinateur"])
    return equipement.get_resistance(plateau["ordinateur"])

def a_perdu(plateau):
    """indique si le joueur de ce plateau a perdu

    Args:
        plateau (dict): un plateau

    Returns:
        bool: True si le plateau est perdant et False sinon
    """
    return get_nb_ordis_restants(plateau) <= 0 and get_resistance_serveur(plateau) <= 0

def est_sur_le_plateau(plateau, position):
    """Indique si la position est bien sur le plateau

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        [boolean]: True si la position est bien sur le plateau
    """
    (ligne,colonne) = position
    return get_taille(plateau)-1 >= colonne >= 0 and get_taille(plateau)-1 >= ligne >= 0 

def deplacer_avatar(plateau, direction):
    """déplace l'avatar dans une des directions 'DD', 'GG', 'HH', 'BB', 'HD', 'HG', 'BD', 'BG'
       Le joueur sera pénalisé de 5 points si l'avatar ne peut pas se déplacer
       dans la direction indiquée et il gagnera 5 points par trojans détruits

    Args:
        plateau (dict): un plateau
        direction (str): la direction vers où se déplace l'avatar
    """
    dico_dir = {'DD' : (0,1), "GG" : (0,-1), "HH" : (-1,0) , "HD" : (-1,1), "BG": (1,-1), "BB" : (1,0),
                "BD" : (1,1), "HG" : (-1,-1)}

    (ligne1,colonne1) = dico_dir[direction]
    (ligne,colonne) = get_pos_avatar(plateau)
    if est_sur_le_plateau(plateau, (ligne+ligne1, colonne+colonne1)):
        joueur.ajouter_points(plateau["joueur"],TROJAN_ELIMINE*len(case.get_trojans(plateau["matrice"][(ligne+ligne1, colonne+colonne1)])))
        plateau["matrice"][(ligne+ligne1, colonne+colonne1)]["trojan_present"] = []
        case.enlever_avatar(plateau["matrice"][get_pos_avatar(plateau)])
        poser_avatar(plateau,ligne+ligne1,colonne+colonne1)
    else: 
        joueur.ajouter_points(plateau["joueur"], PENALITE)


def preparer_trojan(plateau, destinataire, type_t):
    """creer un nouveau trojan appartenant au joueur du plateau, à destination d'un des voisins
        et avec un type défini

    Args:
        plateau (dict): un plateau
        destinataire (str): une des direction D, G ou H
        type (int): un nombre entre 0 et taille-1
    """
    if destinataire == "H":
        direction = "B"
        type_t = abs(type_t-4)
    else:
        direction = destinataire
    t1 = trojan.creer_trojan(get_id_joueur(plateau),type_t,direction)
    plateau["trojan_sortant"][destinataire].append(t1)


def diriger_trojan(plateau):
    """Dirige tous les trojan adjascents à des données personnelles vers ces données personnelles

    Args:
        plateau (dict): un plateau
    """
    dico_dir_inverse = {(0,1) : "G", (0,-1) : "D", (-1,0) : "B" , (1,0) : "H"}
    for ligne,colonne in get_matrice(plateau):
        if case.get_protection(get_case(plateau,ligne,colonne)) != None:
            if protection.get_type(case.get_protection(get_case(plateau,ligne,colonne))) == protection.DONNEES_PERSONNELLES:
                for (ligne_voisin,colonne_voisin) in dico_dir_inverse.keys():
                    if est_sur_le_plateau(plateau,(ligne+ligne_voisin,colonne+colonne_voisin)):
                        trojan_case_voisin = case.get_trojans(get_case(plateau,ligne+ligne_voisin,colonne+colonne_voisin))
                        if trojan_case_voisin != []:
                            for trojans in trojan_case_voisin:
                                trojan.set_direction(trojans,dico_dir_inverse[(ligne_voisin,colonne_voisin)])






# FIN DES FONCTIONS A IMPLEMENTER AVANT SEMAINE DE PROJET
DICO_DIR_ADD = {"D" : (0,1), "G" : (0,-1), "H" : (-1,0), "B" : (1,0)}
def deplacer_trojan_phase1(plateau):
    """déplace les chevaux de Troie présents sur les cases en fonction de leur direction
       cette fonction introduit aussi les trojans entrants du plateau
       Si le déplacement provoque la sortie du trojan du plateau, ce trojans est placés
       dans les trojans sortants du plateau

    Args:
        plateau (dict): un plateau
    """
    for (ligne,colonne) in get_matrice(plateau):
        trojan_case = case.get_trojans(get_matrice(plateau)[(ligne,colonne)])
        for trojans in trojan_case:
            direction = trojan.get_direction(trojans)
            ligne_add = DICO_DIR_ADD[direction][0]
            colonne_add = DICO_DIR_ADD[direction][1]
            if est_sur_le_plateau(plateau,(ligne+ligne_add,colonne+colonne_add)):
                case.ajouter_trojan(plateau["matrice"][ligne+ligne_add,colonne+colonne_add],trojans)
            else:
                plateau["trojan_sortant"][trojan.get_direction(trojans)].append(trojans)
                if trojan.get_direction(trojans) == "H":
                    trojan.set_type(trojans,abs(colonne-4))
                    trojan.set_direction(trojans,"B")
                else:
                    trojan.set_type(trojans,ligne)

                
    for element in plateau["trojan_recu"]:
        if trojan.get_direction(element) == "B":
            case.ajouter_trojan(get_case(plateau,0,trojan.get_type(element)),element)
            get_case(plateau,0,trojan.get_type(element))["trojan_present"] = []
        elif trojan.get_direction(element) == "G":
            case.ajouter_trojan(get_case(plateau,trojan.get_type(element),get_taille(plateau)-1),element)
            get_case(plateau,0,trojan.get_type(element))["trojan_present"] = []
        elif trojan.get_direction(element) == "D":
            case.ajouter_trojan(get_case(plateau,trojan.get_type(element),0),element)
            get_case(plateau,0,trojan.get_type(element))["trojan_present"] = []

    plateau["trojan_recu"] = []
    return plateau
        


def appliquer_dpo(plateau, ligne_protection, colonne_protection, les_trojans):
    """Cette fonction applique l'effet de la protection Data Protection Officer
       posée en ligne_protection, colonne_protection à une liste de trojans

    Args:
        plateau (dict): un plateau
        ligne_protection (int): la ligne de la protection
        colonne_protection (int): la colonne de la protection
        les_trojans (list): une liste de trojans
    """
    dir = {"H": (-1,0),"B" : (1,0), "G" : (0,-1), "D" : (0,1)}
    for trojans in les_trojans:
        trojan.inverser_direction(trojans)
        d = trojan.get_direction(trojans)
        if est_sur_le_plateau(plateau,(ligne_protection+dir[d][0],colonne_protection+dir[d][1])):
            case.ajouter_trojan(get_case(plateau,ligne_protection+dir[d][0],colonne_protection+dir[d][1]),trojans)
        else:
            plateau["trojan_sortant"][d].append(trojans)


def appliquer_firewall_bdhg(plateau, ligne_protection, colonne_protection, les_trojans):
    """Cette fonction applique l'effet de la protection firewall basdroit-hautgauche
       posée en ligne_protection, colonne_protection à une liste de trojans

    Args:
        plateau (dict): un plateau
        ligne_protection (int): la ligne de la protection
        colonne_protection (int): la colonne de la protection
        les_trojans (list): une liste de trojans
    """
    for element in les_trojans:
        trojan.changer_direction_angle_bdhg(element)
        direction_t = trojan.get_direction(element)
        if est_sur_le_plateau(plateau,(ligne_protection+DICO_DIR_ADD[direction_t][0],colonne_protection+DICO_DIR_ADD[direction_t][1])):
            case.ajouter_trojan(get_case(plateau,ligne_protection+DICO_DIR_ADD[direction_t][0],colonne_protection+DICO_DIR_ADD[direction_t][1]),element)
            
        else:
            if trojan.get_direction(element) == "H":
                trojan.set_type(element,-colonne_protection)
                trojan.set_direction(element,"B")
            else:
                trojan.set_type(element,ligne_protection)
            plateau["trojan_sortant"][direction_t].append(element)


def appliquer_firewall_bghd(plateau, ligne_protection, colonne_protection, les_trojans):
    """Cette fonction applique l'effet de la protection firewall basgauche-hautdroit
       posée en ligne_protection, colonne_protection à une liste de trojans

    Args:
        plateau (dict): un plateau
        ligne_protection (int): la ligne de la protection
        colonne_protection (int): la colonne de la protection
        les_trojans (list): une liste de trojans
    """
    for element in les_trojans:
        trojan.changer_direction_angle_bghd(element)
        direction_t = trojan.get_direction(element)
        if est_sur_le_plateau(plateau,(ligne_protection+DICO_DIR_ADD[direction_t][0],colonne_protection+DICO_DIR_ADD[direction_t][1])):
            case.ajouter_trojan(get_case(plateau,ligne_protection+DICO_DIR_ADD[direction_t][0],colonne_protection+DICO_DIR_ADD[direction_t][1]),element)
        else:
            if trojan.get_direction(element) == "H":
                trojan.set_type(element,-colonne_protection)
            else:
                trojan.set_type(element,ligne_protection)
            plateau["trojan_sortant"][direction_t].append(element)



def appliquer_antivirus(plateau, les_trojans):
    """Cette fonction applique l'effet de la protection antivirus
       posée en ligne_protection, colonne_protection à une liste de trojans

    Args:
        plateau (dict): un plateau
        les_trojans (list): une liste de trojans
    """
    " "
   

    for trojans in les_trojans: 
        createur_t = trojan.get_createur(trojans)
        if id_joueur_droite(plateau) == createur_t:
            plateau["trojan_sortant"]["D"].append(trojans)
            trojan.set_direction(trojans,"D")
        elif id_joueur_gauche(plateau) == createur_t:
            plateau["trojan_sortant"]["G"].append(trojans)
            trojan.set_direction(trojans,"G")
        elif id_joueur_haut(plateau) == createur_t:
            plateau["trojan_sortant"]["H"].append(trojans)
            trojan.inverser_direction(trojans)
            trojan.set_type(trojans,abs(trojan.get_type(trojans)-4))
        trojan.set_createur(trojans,get_id_joueur(plateau))




def deplacer_trojan_phase2(plateau):
    """Finalise le déplacements des chevaux de Troies en appliquant les protections

    Args:
        plateau (dict): un plateau

    Returns:
        dict: un dictionnaire donnant pour chaque joueur le nombre de trojans restant sur le plateau
    """
    for ligne,colonne in get_matrice(plateau):
        if (case.get_trojans_entrants(get_case(plateau,ligne,colonne))) != []:
            for trojans in case.get_trojans_entrants(get_case(plateau,ligne,colonne)):

                if case.get_protection(get_case(plateau,ligne,colonne)) != None: 
                    protection_case = protection.get_type(case.get_protection(get_case(plateau,ligne,colonne)))
                    if protection_case == protection.ANTIVIRUS:
                        appliquer_antivirus(plateau,case.get_trojans_entrants(get_case(plateau,ligne,colonne)))
                        ajouter_points(plateau,TROJAN_PIEGE)
                        case.reinit_trojans_entrants(get_case(plateau,ligne,colonne))

                    elif protection_case == protection.DPO:
                        appliquer_dpo(plateau,ligne,colonne,case.get_trojans_entrants(get_case(plateau,ligne,colonne)))
                        ajouter_points(plateau,TROJAN_PIEGE)
                        case.reinit_trojans_entrants(get_case(plateau,ligne,colonne))

                    elif protection_case == protection.FIREWALL_BDHG:
                        appliquer_firewall_bdhg(plateau,ligne,colonne,case.get_trojans_entrants(get_case(plateau,ligne,colonne)))
                        ajouter_points(plateau,TROJAN_PIEGE)
                        case.reinit_trojans_entrants(get_case(plateau,ligne,colonne))

                    elif protection_case == protection.FIREWALL_BGHD:
                        appliquer_firewall_bghd(plateau,ligne,colonne,case.get_trojans_entrants(get_case(plateau,ligne,colonne)))
                        ajouter_points(plateau,TROJAN_PIEGE)
                        case.reinit_trojans_entrants(get_case(plateau,ligne,colonne))
                elif case.contient_avatar(get_case(plateau,ligne,colonne)):
                    ajouter_points(plateau,TROJAN_ELIMINE)
                    case.reinit_trojans_entrants(get_case(plateau,ligne,colonne))

                elif case.get_serveur(get_case(plateau,ligne,colonne)):
                    if not equipement.est_detruit(case.get_serveur(get_case(plateau,ligne,colonne))):
                        equipement.attaque(case.get_serveur(get_case(plateau,ligne,colonne)))
                        ajouter_points(plateau,EQUIPEMENT_TOUCHE)

                    else:
                        equipement.set_resistance(get_serveur(get_case(plateau,ligne,colonne)),0)
                        
                    case.reinit_trojans_entrants(get_case(plateau,ligne,colonne))


           


    for _ in get_trojans_sortants(plateau,"B"):
        if equipement.get_resistance(plateau["ordinateur"]) > 0 :
            attaquer_ordinateurs(plateau)
            ajouter_points(plateau,EQUIPEMENT_TOUCHE)
    for trojans in get_trojans_sortants(plateau,"H"):
        trojan.set_type(trojans,abs(trojan.get_type(trojans)-4))
        trojan.set_createur(trojans,get_id_joueur(plateau))
        trojan.set_direction(trojans,"B")
    for trojans in get_trojans_sortants(plateau,"G"):
        trojan.set_createur(trojans,get_id_joueur(plateau))

    for trojans in get_trojans_sortants(plateau,"D"):
        trojan.set_createur(trojans,get_id_joueur(plateau))


    res = {1:0,2:0,3:0,4:0}
    for case_plateau in get_matrice(plateau).values():
        case.mettre_a_jour_case(case_plateau)
        for trojans in case.get_trojans(case_plateau):
            res[trojan.get_createur(trojans)] +=1
        if case.get_protection(case_plateau) != None:
            protection.enlever_resistance(case.get_protection(case_plateau))
            if protection.get_resistance(case.get_protection(case_plateau)) <= 0 : 
                case.set_protection(case_plateau,None)

    return res

def reinit_les_sorties(plateau):
    """Reinitialise les sorties du plateau à vide

    Args:
        plateau (dict): un plateau
    """
    plateau["trojan_sortant"] = {"H" : [], "B" : [], "G" : [], "D" : []}


def nb_trojans_prochain(plateau):
    """fonction qui retourne une matrice indiquant combien de trojans se trouveront
       sur chaque case du plateau au prochain tour

    Args:
        plateau (dict): un plateau

    Returns:
        dict: une matrice d'entiers
    """
    matrice_trojan = matrice.creer_matrice(matrice.get_nb_lignes(get_matrice(plateau)),matrice.get_nb_lignes(get_matrice(plateau)),0)
    for (ligne,colonne) in get_matrice(plateau).keys():
        liste_arrivant_case = case.get_trojans_entrants(get_case(plateau,ligne,colonne))
        matrice.set_val(matrice_trojan,ligne,colonne, len(liste_arrivant_case))
    return matrice_trojan


def verification_ordres(ordres) :
    if len(ordres) != 9 and len(ordres) != 6 :  # verif taille 
        return False
    elif ordres[2] != 'A' and ordres[2] != 'P' :  # verif si c'est soit une attaque ou une defense
        return False
    elif ordres[2] == 'A' and len(ordres) != 9 :  # verif taille si c'est une attaque
        return False 
    elif ordres[2] == 'D' and len(ordres) != 6 :  # verif taille si c'est une defense
        return False
    elif ordres[0:2] not in DIRECTIONS_AVATAR :   # verif direction valide
        return False 
    elif ordres[2] == 'A' :
        if ordres[3].isdecimal() or not(ordres[4].isdecimal()) and 0 > int(ordres[4]) or int(ordres[4]) > 4 or ordres[5].isdecimal() or not(ordres[6].isdecimal()) and 0 > int(ordres[6]) or int(ordres[6]) > 4 or ordres[7].isdecimal() or not(ordres[8].isdecimal()) and 0 > int(ordres[8]) or int(ordres[8]) > 4 :  # verif si les numéros de lignes et colonne sont bien des nombres
            return False
        elif ordres[3] not in "HDG" and ordres[5] not in "HDG" and ordres[7] not in "HDG" and 0 > int(ordres[4]) or int(ordres[4]) > 4 and 0 > int(ordres[6]) or int(ordres[6]) > 4 or 0 > int(ordres[8]) or int(ordres[8]) > 4 : # verif la validité des directions des trojans 
                return False 
    elif ordres[2] == 'P' :
        if not(ordres[3].isdecimal()) or not(ordres[4].isdecimal()) or not(ordres[5].isdecimal()) : # verif si les numéros de lignes et de colonnes sont valides
            return False
        elif 0 > int(ordres[3]) or int(ordres[3]) > 4 and 0 > int(ordres[4]) or int(ordres[4]) > 4 and 0 > int(ordres[5]) or int(ordres[5]) > 4 :  # verfie si les numeros de lignes sont bien compris entre 0 et 4.
                return False
    return True  # return True si tout les test sont passés

def executer_ordres(plateau, ordres):
    """Exécute les ordres choisis par le joueur. Les ordres sont donnés sous la forme
       d'une chaine de caractères dont les deux premiers indique le déplacement de l'avatar
       le troisième caractère est
       soit un A pour une attaque
       soit un P pour une protection
       En cas d'attaque, les caractères suivants sont GxHyDz où
                    x y et z sont des chiffres entre 0 et 4 indiquant le numéro de la
                             ligne ou de la colonne où sera envoyé le trojan
       En cas de pose d'une protection les caractère suivants seront trois chiffre tlc où
                    t est le type de la protection
                    l la ligne où poser la protection
                    c la colonne où poser la protection
        Si la chaine est non conforme où l'action non permise le joueur perd 5 points.
        Notez que les points et les pénalités liés au déplacement de l'avatar sont gérés dans
        deplacer_avatar

    Args:
        plateau (dict): un plateau
        ordres (str): une chaine de caractères donnant les ordres du joueur
    """
    reinit_les_sorties(plateau)
    if verification_ordres(ordres):
        deplacer_avatar(plateau,ordres[:2])
        type_m = ordres[2]
        if type_m == "A" :
            attaque = ordres[3:]
            for i in range(0,len(attaque),2):
                type_t = int(attaque[i+1])
                preparer_trojan(plateau,attaque[i],type_t)
        else:
            type = int(ordres[3])
            ligne = int(ordres[4])
            colonne = int(ordres[5])
            poser_protection(plateau,type,ligne,colonne)
    else:
        ajouter_points(plateau,-5)

#-------------------------------------------------------------#
# Fonctions pour la sauvegarde et la restoration d'un plateau #
#-------------------------------------------------------------#


def plateau_2_str(plateau, separateur=";"):
    """sauvegarde un plateau sous la forme d'une chaîne de caractères

    Args:
        plateau (dict): un plateau
        separateur (str, optional): le séparateur utilisé pour séparer les champs
                                    sur une même ligne. Defaults to ";".

    Returns:
        str: la chaîne caractères représentant le plateau
    """
    pos_av = get_pos_avatar(plateau)
    res = "matrice"+separateur+str(get_taille(plateau))+"\n" +\
        "joueur"+separateur+str(get_id_joueur(plateau))+separateur+get_nom_joueur(plateau) +\
        separateur+str(get_points(plateau))+"\n" +\
        "avatar"+separateur+str(pos_av[0])+separateur+str(pos_av[1])+"\n" +\
        "serveur"+separateur+str(get_resistance_serveur(plateau))+"\n" +\
        "ordinateurs"+separateur+str(get_nb_ordis_restants(plateau))+"\n"
    res += "trojans entrants\n"
    for troj in get_trojans_entrants(plateau):
        res += str(trojan.get_createur(troj))+separateur+str(trojan.get_type(troj)) +\
            separateur+str(trojan.get_direction(troj))+"\n"
    res += "trojans sortants\n"
    for dest in "HBDG":
        res += dest+'\n'
        for troj in get_trojans_sortants(plateau, dest):
            res += str(trojan.get_createur(troj))+separateur+str(trojan.get_type(troj)) +\
                separateur+str(trojan.get_direction(troj))+"\n"
    res += "matrice\n"
    taille = get_taille(plateau)
    mat = get_matrice(plateau)
    for ligne in range(taille):
        for colonne in range(taille):
            la_case = matrice.get_val(mat, ligne, colonne)
            if case.get_trojans(la_case) != [] or case.get_trojans_entrants(la_case) != [] or\
                    case.get_protection(la_case) is not None:
                res += "case"+separateur + \
                    str(ligne)+separateur+str(colonne)+"\n"
                protec = case.get_protection(la_case)
                if protec is None:
                    res += "protection\n"
                else:
                    res += "protection"+separateur + \
                        str(protection.get_type(protec))+";" + \
                        str(protection.get_resistance(protec))+"\n"
                res += "trojans presents\n"
                for troj in case.get_trojans(la_case):
                    res += str(trojan.get_createur(troj))+separateur+str(
                        trojan.get_type(troj))+separateur+str(trojan.get_direction(troj))+"\n"
                res += "trojans entrants\n"
                for troj in case.get_trojans_entrants(la_case):
                    res += str(trojan.get_createur(troj))+separateur+str(
                        trojan.get_type(troj))+separateur+str(trojan.get_direction(troj))+"\n"
    return res+'fin plateau\n'


def valider_mot_cle(champs, indice, mot_cle, types_champs):
    """fonction outil permettant de valider un mot clé dans une chaine représentant un plateau

    Args:
        champs (list): liste des champs trouvé sur la ligne
        indice (int): numéro de la ligne dans la chaîne (utilisé pour les message d'erreur)
        mot_cle (str): le mot clé recherché
        types_champs (list): une liste de booléens indiquant les champs qui doivent être
                             convertis en entiers

    Raises:
        Exception: si le nombre de champs ne correspond pas à la longeur des types de champs +1
        Exception: si le mot clé n'est pas le premier champs
        Exception: si une convertion en entier s'est mal passée

    Returns:
        list: la liste des champs associés au mot clé
    """
    if len(champs) != len(types_champs)+1:
        raise Exception("ligne "+str(indice)+": comporte " +
                        str(len(champs))+" champs au lieu de "+str(len(types_champs)+1))

    if champs[0] != mot_cle:
        raise Exception("ligne "+str(indice) +
                        ": ne commence pas par le mot clé '"+mot_cle+"'")
    res = []
    for ind in range(len(types_champs)):
        if types_champs[ind]:
            try:
                res.append(int(champs[ind+1]))
            except:
                raise Exception("ligne "+str(indice)+": le "+str(ind+1) +
                                "eme champs devrait être un entier\n"+str(champs))
        else:
            res.append(champs[ind+1])
    return res


def liste_trojans_from_str(lignes, ind_debut, separateur):
    """récupère une liste de trojans dans les lignes à partir de ind_début

    Args:
        lignes (list): une liste de chaines de caractères correspondant à
            des lignes de la chaines représentant un plateau
        ind_debut (int): indice dans la liste lignes à partir de laquelle
            on recherche les trojans
        separateur (str): le caractère séparateur des champs dans une ligne

    Returns:
        int,list: le numéro de la ligne où s'arrête la liste de trojans, la
                  liste des trojans
    """
    res = []
    num_lig = ind_debut
    while True:
        champs = lignes[num_lig].split(separateur)
        if len(champs) != 3:
            break
        try:
            res.append(trojan.creer_trojan(
                int(champs[0]), int(champs[1]), champs[2]))
        except:
            break
        num_lig += 1
    return num_lig, res


def creer_plateau_from_str(chaine, separateur=';'):
    """recréer un plateau à partir d'une chaine de caractères

    Args:
        chaine (str): la chaine de caractère représentant le plateau
        separateur (str, optional): le caractère séparateur des champs au sein d'une ligne.
                                    Defaults to ';'.

    Raises:
        Des exceptions sont levées en cas d'erreur de format de la chaine d'entrée

    Returns:
        dict: le plateau correcpondant à la chaine de caractères
    """
    lignes = chaine.split("\n")
    # ligne 0
    champs = lignes[0].split(separateur)
    [taille] = valider_mot_cle(champs, 0, "plateau", [True])
    # ligne 1 : le joueur
    champs = lignes[1].split(separateur)
    [id_joueur, nom_joueur, nb_points] = valider_mot_cle(
        champs, 1, "joueur", [True, False, True])
    if id_joueur < 1 or id_joueur > 4:
        raise Exception("ligne 1: l'identifiant du joueur n'est pas correcte")
    # ligne 2 : l'avatar
    champs = lignes[2].split(separateur)
    [ligne_av, colonne_av] = valider_mot_cle(champs, 2, "avatar", [True, True])
    if ligne_av < 0 or ligne_av > taille or colonne_av < 0 or colonne_av > taille:
        raise Exception("ligne 2: la position de l'avatar est incorrecte")
    # ligne 3 serveur
    champs = lignes[3].split(separateur)
    [protec_serveur] = valider_mot_cle(champs, 3, "serveur", [True])
    if protec_serveur < 0 or protec_serveur > 4:
        raise Exception("ligne 3: la protection du serveur est incorrecte")
    # ligne 4 ordinateurs
    champs = lignes[4].split(separateur)
    [protec_ordis] = valider_mot_cle(champs, 4, "ordinateurs", [True])
    if protec_ordis < 0 or protec_ordis > 5:
        raise Exception(
            "ligne 4: la protection des ordinateurs est incorrecte")
    # ligne 5 protection en cours
    champs = lignes[5].split(separateur)
    num_lig = 5
    valider_mot_cle(champs, num_lig, "trojans entrants", [])
    num_lig += 1
    num_lig, trojans_entrants = liste_trojans_from_str(
        lignes, num_lig, separateur)
    champs = lignes[num_lig].split(separateur)
    valider_mot_cle(champs, num_lig, "trojans sortants", [])
    num_lig += 1
    trojans_sortants = {}
    for direct in "HBDG":
        champs = lignes[num_lig].split(separateur)
        if len(champs) != 1:
            raise Exception("ligne "+str(num_lig)+": contient " +
                            str(len(champs))+" champs au lieu de 1")
        if champs[0] != direct:
            raise Exception("ligne "+str(num_lig) +
                            ": direction "+direct+" manquante")
        num_lig += 1
        num_lig, liste_trojans = liste_trojans_from_str(
            lignes, num_lig, separateur)
        trojans_sortants[direct] = liste_trojans

    le_plateau = creer_plateau(id_joueur, nom_joueur, taille)
    ajouter_points(le_plateau, nb_points)
    case.enlever_avatar(matrice.get_val(
        get_matrice(le_plateau), taille//2, taille//2))
    poser_avatar(le_plateau, ligne_av, colonne_av)
    set_trojans_entrants(le_plateau, trojans_entrants)
    for direct in trojans_sortants:
        set_trojans_sortants(le_plateau, direct, trojans_sortants[direct])
    equipement.set_resistance(get_serveur(le_plateau), protec_serveur)
    equipement.set_resistance(get_ordis(le_plateau), protec_ordis)
    champs = lignes[num_lig].split(separateur)
    valider_mot_cle(champs, num_lig, "matrice", [])
    num_lig += 1
    la_matrice = get_matrice(le_plateau)
    while True:
        champs = lignes[num_lig].split(separateur)
        if champs[0] != "case":
            break
        if len(champs) != 3:
            raise Exception("ligne "+str(num_lig)+": contient " +
                            str(len(champs))+" champs au lieu de 3")
        try:
            ligne = int(champs[1])
            colonne = int(champs[2])
            if ligne < 0 or ligne >= taille or colonne < 0 or colonne >= taille:
                raise Exception()
        except:
            raise Exception("ligne "+str(num_lig) +
                            ": la spécification de la case n'est pas correcte")
        num_lig += 1
        champs = lignes[num_lig].split(separateur)
        protect = None
        if len(champs) != 1 and len(champs) != 3:
            raise Exception("ligne "+str(num_lig) +
                            " : la ligne doit comporter 1 ou 3 champs")
        if champs[0] != "protection":
            raise Exception("ligne "+str(num_lig) +
                            " : mot cle protection manquant")
        if len(champs) == 3:
            try:
                type_p = int(champs[1])
                res_p = int(champs[2])
                if type_p < 0 or type_p >= protection.PAS_DE_PROTECTION or res_p > 2 or res_p <= 0:
                    raise Exception()
                protect = protection.creer_protection(type_p, res_p)
            except:
                raise Exception("ligne "+str(num_lig) +
                                " : la protection est mal spécifiée")
        num_lig += 1
        champs = lignes[num_lig].split(separateur)
        valider_mot_cle(champs, num_lig, "trojans presents", [])
        num_lig += 1
        num_lig, trojan_cp = liste_trojans_from_str(
            lignes, num_lig, separateur)
        champs = lignes[num_lig].split(separateur)
        valider_mot_cle(champs, num_lig, "trojans entrants", [])
        num_lig += 1
        num_lig, trojan_ce = liste_trojans_from_str(
            lignes, num_lig, separateur)
        la_case = matrice.get_val(la_matrice, ligne, colonne)
        case.set_les_trojans(la_case, trojan_cp, trojan_ce)
        if protect is not None:
            case.set_protection(la_case, protect)
    champs = lignes[num_lig].split(separateur)
    valider_mot_cle(champs, num_lig, "fin plateau", [])
    return le_plateau


def sauver_plateau(plateau, nom_fic, separateur=';'):
    """sauvegarde un plateau dans un fichier

    Args:
        plateau (dict): un plateau
        nom_fic (str): le nom du fichier où sauvegarder le plateau
        separateur (str, optional): caractère séparateur utiliser dans le fichier. Defaults to ';'.
    """
    with open(nom_fic, "w") as fic:
        fic.write(plateau_2_str(plateau, separateur))


def charger_plateau(nom_fic, separateur=";"):
    """charge un plateau sauvegardé dans un fichier

    Args:
        nom_fic (str): nom du fichier
        separateur (str, optional): caractère séparateur utiliser dans le fichier. Defaults to ";".

    Returns:
        plateau: un plateau
    """
    with open(nom_fic) as fic:
        chaine = fic.read()
        return creer_plateau_from_str(chaine, separateur)
