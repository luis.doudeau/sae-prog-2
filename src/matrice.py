"""
             Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module matrice.py
    Ce module de gestion des matrices
"""


def creer_matrice(nb_lig, nb_col, val_defaut=None):
    """créer une matrice contenant nb_lig lignes et nb_col colonnes avec
       pour valeur par défaut val_defaut

    Args:
        nb_lig (int): un entier strictement positif
        nb_col (int): un entier strictement positif
        val_defaut (Any, optional): La valeur par défaut des éléments de la matrice.
                                    Defaults to None.
    Returns:
        dict: la matrice
    """
    matrice = dict()
    for ligne in range(nb_lig):
        for colonne in range(nb_col):
            matrice[(ligne,colonne)] = val_defaut
    return matrice

def get_nb_lignes(matrice):
    """retourne le nombre de lignes de la matrice

    Args:
        matrice (dict): une matrice

    Returns:
        int: le nombre de lignes de la matrice
    """
    nb_lignes = None
    for (ligne,_) in matrice.keys():
        if nb_lignes is None or nb_lignes < ligne:
            nb_lignes = ligne
    return nb_lignes+1

def get_nb_colonnes(matrice):
    """retourne le nombre de colonnes de la matrice

    Args:
        matrice (dict): une matrice

    Returns:
        int: le nombre de colonnes de la matrice
    """
    nb_colonne = None
    for (_,colonne) in matrice.keys():
        if nb_colonne is None or nb_colonne < colonne:
            nb_colonne = colonne
    return nb_colonne+1

def get_val(matrice, lig, col):
    """retourne la valeur en lig, col de la matrice

    Args:
        matrice (dict): une matrice
        lig (int): numéro de la ligne (en commençant par 0)
        col (int): numéro de la colonne (en commençant par 0)

    Returns:
        Any: la valeur en lig, col de la matrice
    """
    return matrice[(lig,col)]


def set_val(matrice, lig, col, val):
    """stocke la valeur val en lig, col de la matrice

    Args:
        matrice (dict): une matrice
        lig (int): numéro de la ligne (en commençant par 0)
        col (int): numéro de la colonne (en commençant par 0)
        val (Any): la valeur à stocker
    """
    matrice[(lig,col)] = val

def voisin(ligne, colonne) : 
    return [(ligne-1, colonne), (ligne+1, colonne), (ligne, colonne-1), (ligne, colonne+1), (ligne-1, colonne-1), (ligne+1, colonne-1), (ligne-1, colonne+1), (ligne+1, colonne+1)] 

def max_matrice(matrice, interdits={}):
    """retourne la liste des coordonnées des cases contenant la valeur la plus grande de la matrice
        Ces case ne doivent pas être parmi les interdits.

    Args:
        matrice (dict): une matrice
        interdits (set): un ensemble de tuples (ligne,colonne) de case interdites. Defaults to set

    Returns:
        list: la liste des coordonnées de cases de valeur maximale dans la matrice (hors cases interdites)
    """
    liste_max = []
    max = 0
    for (x,y),valeur in matrice.items():
        if (x,y) not in interdits and valeur > max:
            max = valeur
            liste_max = [(x,y)]
        elif (x,y) not in interdits and valeur == max:
            liste_max.append((x,y))
    return liste_max

DICO_DIR = {(-1, -1): 'HG', (-1, 0): 'HH', (-1, 1): 'HD', (0, -1): 'GG',
            (0, 1): 'DD', (1, -1): 'BG', (1, 0): 'BB', (1, 1): 'BD', (0, 0): 'BB'}

def est_dans(matrice,ligne,colonne):
    return (ligne,colonne) in matrice.keys()

def direction_max_voisin(matrice, ligne, colonne):
    """retourne la liste des directions qui permettent d'aller vers la case voisine de 
       la case (ligne,colonne) la plus grande. Le résultat doit aussi contenir la 
       direction qui permet de se rapprocher du milieu de la matrice
       si ligne,colonne n'est pas le milieu de la matrice

    Args:
        matrice (dict): une matrice
        ligne (int): le numéro de la ligne de la case considérée
        colonne (int): le numéro de la colonne de la case considérée

    Returns:
        str: deux lettres indiquant la direction DD -> droite , HD -> Haut Droite,
                                                 HH -> Haut, HG -> Haut gauche,
                                                 GG -> Gauche, BG -> Bas Gauche, BB -> Bas
    """
    val_max = -1
    liste = []
    direction  = DICO_DIR
    for (position_x,position_y),str in direction.items():
        if est_dans(matrice,ligne+position_x,colonne+position_y):
            if get_val(matrice,ligne+position_x,colonne+position_y) > val_max and (ligne+position_x,colonne+position_y) != (ligne,colonne):
                liste = [str]
                val_max = get_val(matrice,ligne+position_x,colonne+position_y)
            elif get_val(matrice,ligne+position_x,colonne+position_y) == val_max and (ligne+position_x,colonne+position_y) != (ligne,colonne):
                liste.append(str)
    return liste